import React, { useEffect } from "react"
import gsap from "gsap"

import { ManaSymbol } from "./ManaSymbol"

export const Card = (props) => {
    useEffect(() => {
        const fadeUpEl = document.querySelectorAll(".fadeup")

        if (props.showCard) {
            let tl = gsap.timeline(
                {
                    defaults: { duration: 1 },
                },
                { ease: "expo.out" }
            )

            tl.to(fadeUpEl, {
                transform: "translateY(0%)",
                opacity: 1,
                stagger: 0.1,
            })
        }
    }, [props.showCard])

    return (
        <div className="card">
            <div className="card__image fadeup">
                <img
                    src={props.image ? props.image : "/images/placeholder.jpg"}
                    alt="tmc art"
                />
            </div>
            <div className="card__details">
                <div className="card__details-row fadeup col-2">
                    <div>
                        <p>{props.name}</p>
                        <p>Card Name</p>
                    </div>
                    <div className="manacost">
                        <p>
                            {props.manacost ? (
                                <ManaSymbol manacost={props.manacost} />
                            ) : (
                                "Not Available"
                            )}
                        </p>
                        <p>Mana Cost</p>
                    </div>
                </div>
                <div className="card__details-row fadeup col-1">
                    <div>
                        <p>{props.type}</p>
                        <p>Card Type</p>
                    </div>
                </div>
                <div className="card__details-row fadeup col-1 row-2">
                    <div>
                        <p className="description">{props.text}</p>
                        <p>Description</p>
                    </div>
                    <div>
                        <p>{props.power ? props.power : "Not Available"}</p>
                        <p>Power/Toughness</p>
                    </div>
                </div>
                <div className="card__details-row fadeup col-1">
                    <div>
                        <p>{props.artist}</p>
                        <p>Artist</p>
                    </div>
                </div>
            </div>
        </div>
    )
}
