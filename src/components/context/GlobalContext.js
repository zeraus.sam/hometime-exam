import React, { useState, useEffect } from "react"

export const GlobalContext = React.createContext(null)

const GlobalContextProvider = ({ children }) => {
    const [initialFetch, setInitialFetch] = useState(true)
    const [cardList, setCardList] = useState([])
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        if (initialFetch) {
            console.log("fetching cards. please wait.")
            fetch("https://api.magicthegathering.io/v1/cards", {})
                .then((res) => res.json())
                .then((data) => {
                    setCardList(data.cards)
                    setLoading(false)
                    console.log("done fetching cards.")
                    setInitialFetch(false)
                    console.log("disabled any further api fetching.")
                })
        }
        return () => {
            setInitialFetch(false)
        }
    }, [initialFetch])

    return (
        <GlobalContext.Provider
            value={{
                cardList,
                setCardList,
                loading,
                setLoading,
                initialFetch,
            }}
        >
            {children}
        </GlobalContext.Provider>
    )
}

export default GlobalContextProvider
