import React, { useEffect, useContext, useRef } from "react"
import gsap from "gsap"

import { GlobalContext } from "./context/GlobalContext"

export const Loader = () => {
    const { loading } = useContext(GlobalContext)
    const loaderContainer = useRef(null)
    const loaderImg = useRef(null)
    const loaderText = useRef(null)

    useEffect(() => {
        const loaderImgEl = loaderImg.current

        if (loading) {
            let tl = gsap.timeline(
                {
                    defaults: { duration: 1 },
                },
                { ease: "expo.out" }
            )

            tl.fromTo(
                loaderImgEl,
                { filter: "blur(20px)" },
                { filter: "blur(0px)", duration: 1.5 }
            )
        }
    }, [loading])

    useEffect(() => {
        const loaderEl = loaderContainer.current
        const loaderImgEl = loaderImg.current
        const loaderTextEl = loaderText.current

        if (!loading) {
            let tl = gsap.timeline(
                {
                    defaults: { duration: 0.5 },
                },
                { ease: "expo.out" }
            )

            tl.to(loaderImgEl, {
                opacity: 0,
            })

            tl.to(loaderTextEl, { opacity: 0 }, "<")

            tl.to(loaderEl, { right: "100%" })
        }
    }, [loading])

    return (
        <div className="loader" ref={loaderContainer}>
            <img
                className="loader__img"
                src="/images/mtg-loader.png"
                alt="mtg loader"
                ref={loaderImg}
            />
            <p className="loader__text" ref={loaderText}>
                Getting everything ready. Please wait.
            </p>
        </div>
    )
}
