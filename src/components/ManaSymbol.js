import React, { useState, useEffect } from "react"

export const ManaSymbol = (props) => {
    const [icon, setIcon] = useState([])

    useEffect(() => {
        const formatIcons = Array.from(
            props.manacost.toLowerCase().matchAll(/([A-Za-z\d]+)/g),
            (m) => m[0]
        )
        setIcon(formatIcons)
    }, [props])

    return (
        <>
            {icon.map((item, i) => {
                return <i className={`ms ms-${item}`} key={i}></i>
            })}
        </>
    )
}
