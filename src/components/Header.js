import React, { useContext } from "react"
import classNames from "classnames"

import { ThemeContext } from "./context/ThemeContext"

export const Header = () => {
    const { theme, setTheme } = useContext(ThemeContext)

    return (
        <header className="header">
            <img
                src="https://upload.wikimedia.org/wikipedia/commons/3/3f/Magicthegathering-logo.svg"
                className="header__logo"
                alt="mtg"
            />
            <div className="header__toggle" onClick={() => setTheme(!theme)}>
                <div
                    className={classNames({
                        header__toggler: true,
                        dark: !theme,
                    })}
                />
                <div className="header__toggle-theme">Light</div>
                <div className="header__toggle-theme">Dark</div>
            </div>
        </header>
    )
}
