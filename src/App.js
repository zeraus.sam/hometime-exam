import React, { useState, useContext } from "react"
import classNames from "classnames"

import { Card } from "./components/Card"
import { Loader } from "./components/Loader"
import { Header } from "./components/Header"

import { ThemeContext } from "./components/context/ThemeContext"
import { GlobalContext } from "./components/context/GlobalContext"

const App = () => {
    const [getCard, setGetCard] = useState([])
    const [showCard, setShowCard] = useState(false)
    const { cardList } = useContext(GlobalContext)
    const { theme } = useContext(ThemeContext)

    const getRandomCard = () => {
        const randomCard = Math.floor(Math.random() * cardList.length)
        console.log("fetched card index: ", randomCard)
        setGetCard(cardList[randomCard])
    }

    return (
        <div className={classNames({ main: true, dark: !theme })}>
            <Loader />
            <Header />

            <Card
                image={getCard.imageUrl}
                name={getCard.name}
                manacost={getCard.manaCost}
                type={getCard.type}
                text={getCard.text}
                power={getCard.power}
                artist={getCard.artist}
                showCard={showCard}
            />

            <button
                className="card__button"
                onClick={() => {
                    getRandomCard()
                    setShowCard(true)
                }}
            >
                Show Random Card
            </button>
        </div>
    )
}

export default App
