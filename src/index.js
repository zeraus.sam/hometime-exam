import React from "react"
import ReactDOM from "react-dom"

import App from "./App.js"

import "./sass/main.scss"

import GlobalContextProvider from "./components/context/GlobalContext"
import ThemeContextProvider from "./components/context/ThemeContext.js"

import * as serviceWorker from "./serviceWorker"

ReactDOM.render(
    <React.StrictMode>
        <GlobalContextProvider>
            <ThemeContextProvider>
                <App />
            </ThemeContextProvider>
        </GlobalContextProvider>
    </React.StrictMode>,
    document.getElementById("root")
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
